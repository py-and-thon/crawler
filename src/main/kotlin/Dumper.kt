import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.io.FileReader

fun main(args: Array<String>) {
    Database.connect("jdbc:postgresql://localhost:5432/vkdb", "org.postgresql.Driver", "rita", "123456789")

    val categories = listOf(
        "legal" to "Юриспруденция",
        "biology" to "Биология",
        "medicine" to "Медицина",
        "physics" to "Физика",
        "chemistry" to "Химия",
        "math" to "Математика",
        "history" to "История",
        "philosophy" to "Философия",
        "economics" to "Экономика",
        "geo" to "География",
        "lingua" to "Филология"
    )
    val cities = listOf(
        "moscow" to "Москва",
        "spb" to "Санкт-Петербург",
        "nizhny-novgorod" to "Нижний Новгород",
        "kazan" to "Казань",
        "omsk" to "Омск",
        "ekaterinburg" to "Екатеринбург",
        "samara" to "Самара",
        "novosibirsk" to "Новосибирск",
        "ufa" to "Уфа",
        "naberezhnye-%D1%81helny" to "Набережные Челны",
        "novokuzneck" to "Новокузнецк"
    )

    val events = Gson().fromJson<List<Event>>(FileReader(File("test.json")), object : TypeToken<List<Event>>() {}.type)
        .map {
            it.description = it.description.removePrefix("Описание встречи ")
            it
        }
    transaction {
        SchemaUtils.create(CategoriesDAO, CityDao, EventsDAO)

        CategoriesDAO.batchInsert(categories) {
            this[CategoriesDAO.key] = it.first
            this[CategoriesDAO.name] = it.second
        }

        CityDao.batchInsert(cities) {
            this[CityDao.key] = it.first
            this[CityDao.name] = it.second
        }

        for (event in events) {
            val categoryId =
                CategoriesDAO.select { CategoriesDAO.key eq event.categoryKey!! }.map { it[CategoriesDAO.id] }.first()
            val cityId = CityDao.select { CityDao.key eq event.cityKey!! }.map { it[CityDao.id] }.first()

            EventsDAO.insert {
                it[EventsDAO.categoryId] = categoryId
                it[EventsDAO.cityId] = cityId
                it[EventsDAO.description] = event.description
                it[EventsDAO.imageUrl] = event.imageUrl
                it[EventsDAO.url] = event.url
                it[EventsDAO.name] = event.title

            }
        }

    }
}