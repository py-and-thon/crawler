import com.google.gson.Gson
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.get
import org.jsoup.Jsoup

val client = HttpClient {
    install(JsonFeature)
}

data class Event(
    val title: String, var description: String, val url: String, val imageUrl: String,
    var categoryKey: String? = null, var categoryText: String? = null,
    var cityKey: String? = null, var cityText: String? = null
)

suspend fun main(args: Array<String>) {
    val categories = listOf(
        "legal" to "Юриспруденция",
        "biology" to "Биология",
        "medicine" to "Медицина",
        "physics" to "Физика",
        "chemistry" to "Химия",
        "math" to "Математика",
        "history" to "История",
        "philosophy" to "Философия",
        "economics" to "Экономика",
        "geo" to "География",
        "lingua" to "Филология"
    )
    val cities = listOf(
        "moscow" to "Москва",
        "spb" to "Санкт-Петербург",
        "nizhny-novgorod" to "Нижний Новгород",
        "kazan" to "Казань",
        "omsk" to "Омск",
        "ekaterinburg" to "Екатеринбург",
        "samara" to "Самара",
        "novosibirsk" to "Новосибирск",
        "ufa" to "Уфа",
        "naberezhnye-%D1%81helny" to "Набережные Челны",
        "novokuzneck" to "Новокузнецк"
    )

    val events = categories.map { category ->
        cities.map { city ->
            executeAlways {
                downloadCategory(category.first, category.second, city.first, city.second)
            }
        }
    }.flatten().flatten()
    println(events)
    println(Gson().toJson(events))
}

suspend fun downloadCategory(
    key: String?,
    readableText: String?,
    cityKey: String?,
    cityText: String?,
    page: Int? = null
): List<Event> {
    val url =
        "https://theoryandpractice.ru/seminars/${cityKey?.let { "$it/" } ?: ""}science${key?.let { "/$it" }
            ?: ""}${page?.let { "?page=$it" } ?: ""}"
    println("!! Download: $url")
    val result = client.get<String>(url)
    val json = Gson().fromJson(result, Map::class.java)
    val seminars = json["seminars"] as ArrayList<Map<*, *>>
    val currentSeminars = seminars.map { it["content"] as String }
        .map { Jsoup.parse(it).body() }
        .map {
            it.getElementsByClass("preview-box-platform-link")[0].attr("href") to it.getElementsByClass("preview-box-platform-img")[0].attr(
                "data-original"
            )
        }
        .map { downloadEvent(it.first, it.second) }
        .map {
            it.apply {
                categoryKey = key
                categoryText = readableText
                it.cityKey = cityKey
                it.cityText = cityText
            }
        }
    if (currentSeminars.isNotEmpty()) {
        return currentSeminars.plus(downloadCategory(key, readableText, cityKey, cityText, (page ?: 1) + 1))
    } else {
        return currentSeminars
    }
}

suspend fun downloadEvent(path: String, imageUrl: String): Event {
    val url = "https://theoryandpractice.ru$path"
    println(url)
    val event = Jsoup.connect(url).get()
    val title = event.getElementsByClass("platform-header-title")[0].text()
    val description = event.getElementsByAttributeValue("itemprop", "description")[0].text()
    return Event(title, description, url, imageUrl)

}

suspend fun <T> executeAlways(block: suspend () -> T): T {
    return try {
        block.invoke()
    } catch (e: Exception) {
        println("executeAlways")
        executeAlways(block)
    }
}