import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object CityDao : Table("cities") {
    val id: Column<Int> = integer("id").primaryKey().autoIncrement()
    val name: Column<String> = text("name")
    val key: Column<String> = text("key").uniqueIndex()
}

object CategoriesDAO : Table("categories") {
    val id: Column<Int> = integer("id").primaryKey().autoIncrement()
    val name: Column<String> = text("name")
    val key: Column<String> = text("key").uniqueIndex()
}

object EventsDAO : Table("events") {
    val id: Column<Int> = integer("id").primaryKey().autoIncrement()
    val name: Column<String> = text("name")
    val description: Column<String> = text("description")
    val cityId: Column<Int> = integer("city_id").references(CityDao.id)
    val imageUrl: Column<String> = text("image_url")
    val url: Column<String> = text("url")
    val categoryId: Column<Int> = integer("category_id").references(CategoriesDAO.id)
    val rate: Column<Float> = float("rate")
}

object EgeBallDao : Table("egeball") {
    val id: Column<Int> = integer("id").primaryKey().autoIncrement()
    val eduName: Column<String> = text("edu_name")
    val globalId: Column<Int> = integer("global_id")
    val year: Column<String> = text("year")
    val area: Column<String> = text("area")
    val district: Column<String> = text("district")
    val over220: Column<Int> = integer("over_220")
    val under220: Column<Int> = integer("under_160")
    val hardK: Column<Float?> = float("hardk").nullable()
}

object SiriusDAO : Table("sirius_dataset_2") {
    val id: Column<Int> = integer("id").primaryKey().autoIncrement()
    val userid: Column<Int> = integer("userid")
    val achievement: Column<String> = text("achievement")
    val categoryText: Column<String> = text("category_text")
    val school: Column<String> = text("school")
    val schoolClass: Column<Float> = float("school_class")
    val regionText: Column<String> = text("region_text")
    val achievementType: Column<String> = text("achievement_type")
    val regionKey: Column<String> = text("region_key")
    val categoryKey: Column<String> = text("category_key")
    val schoolId: Column<Int?> = integer("school_id").nullable()
    val schoolDiff: Column<Int?> = integer("school_diff").nullable()
    val hardK: Column<Float> = float("hardk")
}