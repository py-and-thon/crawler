import com.opencsv.CSVReader
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.FileReader
import java.util.concurrent.Executors

val categories = listOf(
    "legal" to "Юриспруденция",
    "biology" to "Биология",
    "medicine" to "Медицина",
    "physics" to "Физика",
    "chemistry" to "Химия",
    "math" to "Математика",
    "history" to "История",
    "philosophy" to "Философия",
    "economics" to "Экономика",
    "geo" to "География",
    "lingua" to "Филология"
)
val executor = Executors.newFixedThreadPool(10)

fun main(args: Array<String>) {
    Database.connect("jdbc:postgresql://localhost:5432/vkdb", "org.postgresql.Driver", "rita", "123456789")
    processTable()
}

fun removeDuplicate() {
    transaction {
        val events = EventsDAO.selectAll().map { it[EventsDAO.id] to it[EventsDAO.name] }
        val nameSet = HashSet<String>()
        events.forEach { pair ->
            if (nameSet.contains(pair.second.toLowerCase().trim())) {
                EventsDAO.deleteWhere { EventsDAO.id eq pair.first }
            } else {
                nameSet.add(pair.second.toLowerCase().trim())
            }
        }
    }
}

fun processTable() {
    val csv = CSVReader(FileReader("sirius.csv")).use { it.readAll() }
    val title = csv.first()
    csv.removeAt(0)
    transaction {
        csv.map {
            it[1].toInt() to it[(title.size - 1)]
        }.forEach { pair ->
            EventsDAO.update({ EventsDAO.id eq pair.first }) {
                it[EventsDAO.rate] = 31 - pair.second.toFloat() * 0.0428571429f
            }
        }
    }

}

fun siriusCalc() {
    val sirius = transaction {
        SiriusDAO.selectAll().map { it[SiriusDAO.userid] to it[SiriusDAO.schoolId] }.distinctBy { it.first }
    }
    sirius.forEachIndexed { index, pair ->
        executor.submit {
            transaction {
                val achievCount = Math.min(SiriusDAO.select { SiriusDAO.userid eq pair.first }.count(), 10)
                val classC = SiriusDAO.select {SiriusDAO.userid eq pair.first}.map { it[SiriusDAO.schoolClass] }.max()
                println("$index | $achievCount")
                val schoolHard = pair.second?.let { schoolid ->
                    EgeBallDao.select { EgeBallDao.id eq schoolid }.map {
                        it[EgeBallDao.hardK]
                    }.firstOrNull()
                }
                SiriusDAO.update({ SiriusDAO.userid eq pair.first }) {
                    it[SiriusDAO.hardK] = ((schoolHard ?: 0f) + achievCount.toFloat()) + (classC ?: 0f)
                }
            }
        }
    }
}

fun egeBallCalc() {
    transaction {
        EgeBallDao.update {
            it[EgeBallDao.hardK] = 1f
        }
        val preCalc = EgeBallDao.select { EgeBallDao.over220 greater 20 }
            .map { (it[EgeBallDao.over220].toFloat() / (it[EgeBallDao.under220].toFloat() + it[EgeBallDao.over220])) * 20 to it[EgeBallDao.id] }

        preCalc.map {
            it.first to it.second
        }.forEach { pair ->
            EgeBallDao.update({ EgeBallDao.id eq pair.second }) {
                it[EgeBallDao.hardK] = pair.first
            }
        }
    }
}

fun calculateSchools() {
    transaction {
        val schools = EgeBallDao.selectAll().map { it[EgeBallDao.eduName] to it[EgeBallDao.id] }
        val siriusSchool =
            SiriusDAO.select { SiriusDAO.schoolId eq (null as Int?) }.map { it[SiriusDAO.school] to it[SiriusDAO.id] }

        siriusSchool.forEach { pair ->
            processSchool(pair, schools)
        }
    }
}

fun processSchool(schoolSirius: Pair<String, Int>, schoolEge: List<Pair<String, Int>>) {
    transaction {
        val name = schoolSirius.first
        val id = schoolSirius.second
        val bestComparasion =
            schoolEge.map { editdist(it.first, name) to it.second }.sortedBy { it.first }.firstOrNull()
        bestComparasion?.let {
            val diff = it.first
            val schoolEge = it.second
            SiriusDAO.update({ SiriusDAO.id eq id }) {
                it[SiriusDAO.schoolDiff] = diff
                it[SiriusDAO.schoolId] = schoolEge
            }
        }
    }
}

fun clearData() {
    transaction {
        SiriusDAO.selectAll().map { it[SiriusDAO.categoryText] to it[SiriusDAO.id] }
            .map { pair ->
                (categories.find { pair.first.contains(it.second, true) }?.first ?: "other") to pair.second
            }.forEach { pair ->
                SiriusDAO.update({ SiriusDAO.id eq pair.second }) {
                    it[SiriusDAO.categoryKey] = pair.first
                }
            }
    }
}

fun editdist(S1: String, S2: String): Int {
    val m = S1.length
    val n = S2.length
    var D1: IntArray
    var D2 = IntArray(n + 1)

    for (i in 0..n)
        D2[i] = i

    for (i in 1..m) {
        D1 = D2
        D2 = IntArray(n + 1)
        for (j in 0..n) {
            if (j == 0)
                D2[j] = i
            else {
                val cost = if (S1[i - 1] != S2[j - 1]) 1 else 0
                if (D2[j - 1] < D1[j] && D2[j - 1] < D1[j - 1] + cost)
                    D2[j] = D2[j - 1] + 1
                else if (D1[j] < D1[j - 1] + cost)
                    D2[j] = D1[j] + 1
                else
                    D2[j] = D1[j - 1] + cost
            }
        }
    }
    return D2[n]
}